<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resetpass extends CI_Controller {
public function __construct()
    {
		parent::__construct();
		$this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
		$this->load->model('Resetpass_model');
		$this->load->model('Appsettings_model', 'app');
        $this->load->library('form_validation');
    }
	
	public function index() {
		$datasettings['appsettings'] = $this->app->getappbyid();

		$this->load->view('nodata',$datasettings);
	}
	
	public function rest($token=null,$idkey=null) {
		$data['user'] = $this->Resetpass_model->check_token($token,$idkey);
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($data['user']) {
			if ($this->form_validation->run() == false) {
	
				$data['appsettings'] = $this->app->getappbyid();
				$this->load->view('resetpass',$data);
			} else {
			$reset = $this->Resetpass_model->resetpass();
			if($reset) {
				$this->Resetpass_model->deletetoken();
	
				$datasettings['appsettings'] = $this->app->getappbyid();

			$this->load->view('success',$datasettings);
			}
			}
		
		} else {

			$datasettings['appsettings'] = $this->app->getappbyid();
			$this->load->view('nodata',$datasettings);
		}
	}
}
