<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categorymerchant extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }

        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->model('Categorymerchant_model', 'cm');
        $this->load->model('Appsettings_model', 'app');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }

    public function index()
    {
        $data['catmer'] = $this->cm->getallcm();
        $data['fitur'] = $this->cm->getfiturmerchant();

		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('categorymerchant/index', $data);
        $this->load->view('includes/footer');
    }

    public function tambahcm()
    {


        $this->form_validation->set_rules('nama_kategori', 'nama_kategori', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
  
            if ($_FILES['image']['name']) {

                $cfgFile= array(
                        'file_name' 	=> 'name',
                        'upload_path' 	=> 'images/kategori/',
                        'allowed_types' => 'jpg|png|jpeg',
                        'max_size'   	=> 10000,
                        'encrypt_name'  => true,
                    );

                $this->load->library('upload', $cfgFile);
                $this->upload->initialize($cfgFile);
                
                if ($this->upload->do_upload('image')) {
                    $foto = html_escape($this->upload->data('file_name'));
                }else{
                    $foto = 'noimage.jpg';
                }
            }

            $data = [
                'nama_kategori'     => html_escape($this->input->post('nama_kategori', TRUE)),
                'id_fitur'          => html_escape($this->input->post('id_fitur', TRUE)),
                'status_kategori'   => html_escape($this->input->post('status_kategori', TRUE)),
                'img'               => $foto
            ];

            $this->cm->tambahcm($data);
            $this->session->set_flashdata('tambah', 'Category Merchant Has Been Added');
            redirect('categorymerchant');
        }
    }

    public function hapus($id)
    {
        $data = $this->cm->getKatbyid($id);
        $gambar = $data['img'];
        unlink('images/kategori/' . $gambar);
        $this->cm->hapuscm($id);
        $this->session->set_flashdata('hapus', 'Category Merchant Has Been Deleted');
        redirect('categorymerchant');
    }


    public function ubahcm()
    {
        $this->form_validation->set_rules('nama_kategori', 'nama_kategori', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $id = $this->input->post('id_kategori_merchant');
            $data = $this->cm->getKatbyid($id);

            if (@$_FILES['image']['name']) {

                $config['upload_path']      = './images/kategori';
                $config['allowed_types']    = 'gif|jpg|png|jpeg';
                $config['max_size']         = '10000';
                $config['file_name']        = 'name';
                $config['encrypt_name']     = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    $this->session->set_flashdata('hapus', $this->upload->display_errors());
                    redirect('categorymerchant/index');                  
                } else {
                    if ($data['img'] != 'noimage.jpg') {
                        $gambar = $data['img'];
                        unlink('images/kategori/' . $gambar);
                    }
                }
                $img = html_escape($this->upload->data('file_name'));
            }else{
                $img = $data['img'];
            }
            
            $data = [
                'nama_kategori'     => html_escape($this->input->post('nama_kategori', TRUE)),
                'id_fitur'          => html_escape($this->input->post('id_fitur', TRUE)),
                'status_kategori'   => html_escape($this->input->post('status_kategori', TRUE)),
                'img'               => $img
            ];

            $this->cm->ubahcm($data, $id);
            $this->session->set_flashdata('ubah', 'Category Merchant Has Been Updated');
            redirect('categorymerchant');
        }
    }
}
