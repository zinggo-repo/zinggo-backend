<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Catshop extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }

        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->model('Appsettings_model', 'app');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = array(
            'titlePage'   => 'Category Shop',
            'data'        => $this->mod->getData('result','*','category_shop'),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('category_shop/index', $data);
        $this->load->view('includes/footer');
    }

    public function tambah()
    {
        $this->form_validation->set_rules('catname', 'Name Category', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $data = [
                'cat_shop_name'     => html_escape($this->input->post('catname', TRUE)),
            ];

            $this->mod->insertData('category_shop',$data);
            $this->session->set_flashdata('tambah', 'Category Has Been Added');
            redirect('catshop');
        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect('catshop');
        }
    }

    public function hapus($id)
    {
        $query 	= $this->mod->deleteData('category_shop', array('id_cat_shop' => $id));        
		if ($query){
            $this->session->set_flashdata('hapus', 'Category Has Been Deleted');
            redirect('catshop');
        }else{
            $this->session->set_flashdata('error', 'An error occurred while deleting data !');
            redirect('catshop');
        }
    }


    public function ubah()
    {


        $this->form_validation->set_rules('catname', 'Name Category', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $id = $this->input->post('idcat');
            
            $data = [
                'cat_shop_name'     => html_escape($this->input->post('catname', TRUE)),
            ];

            $this->mod->updateData('category_shop',$data, array('id_cat_shop'=>$id));
            $this->session->set_flashdata('ubah', 'Category Has Been Updated');
            redirect('catshop');
        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect('catshop');
        }
    }
}
