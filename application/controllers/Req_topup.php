<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Req_topup extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->model('Appsettings_model', 'app');
        $this->load->model('Notification_model', 'notif');
        $this->load->model('Email_model');
    }

    public function index()
    {
        $data = array(
            'titlePage'   => 'Topup Request Page',
            'app'         => $this->app->getappbyid(),
            'data'  => $this->mod->getData('result','*','request_top_up',null,null,null,null,null,array('id'=>'DESC')),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

		$this->load->view('includes/header', $datasettings);
        $this->load->view('request/index_topup', $data);
        $this->load->view('includes/footer');
    }

    public function uprove($id)
    {
        $getReq = $this->mod->getData('row','*','request_top_up',null,null,null,array('id'=>$id));
        $getSaldo = $this->mod->getData('row','*','saldo',null,null,null,array('id_user'=>$getReq->id_user));
        $saldoAkhir = $getSaldo->saldo + $getReq->nominal;
        $updateSaldo = $this->mod->updateData('saldo',array(
            'saldo'  => $saldoAkhir,
            'update_at' => date('Y-m-d h:i:s')
        ), array('id_user'=>$getReq->id_user));

        $update = $this->mod->updateData('request_top_up',array(
            'status_topup'  => '1',
        ), array('id'=>$id));

        $insert_wallet = $this->mod->insertData('wallet', array(
            'invoice' => $getReq->code_request_top_up,
            'id_user' => $getReq->id_user,
            'jumlah' => $getReq->nominal,
            'bank'  => $getReq->nama_bank,
            'nama_pemilik' => $getReq->atas_nama, 
            'rekening' => $getReq->no_rekening,
            'waktu' => $getReq->tgl_transfer,
            'type' => 'topup',
            'status' => 1
        ));

        $app = $this->mod->getData('row','*','app_settings');

        $title = 'Top up Success';
        $message = 'Your request has been success';
        $topic = get_token($getReq->id_user);

        $this->notif->send_notif_to($title, $message, $topic);

        $subject = 'Request Topup Berhasil !';
        $emailmessage = 'Request Topup telah di tambahkan ke akun kamu, dengan total saldo <strong>Rp.' . number_format($saldoAkhir,0,",",".") . '</strong><br><br>Salam Hormat , ZingGo Team.';
        $host = $app->smtp_host;
        $port = $app->smtp_port;
        $username = $app->smtp_username;
        $password = $app->smtp_password;
        $from = $app->smtp_from;
        $appname = $app->app_name;
        $secure = $app->smtp_secure;
        $address = $app->app_address;
        $linkgoogle = $app->app_linkgoogle;
        $web = $app->app_website;
        $linkimage = base_url(). 'asset/images/' . $app->app_logo;

        $emailuser = get_email($getReq->id_user);

        $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
        $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);
        
        if ($send) {
            $this->session->set_flashdata('success', 'Data Uprove ! Email notification sended..');
            redirect('req_topup/index');
        }
    }

    public function reject($id)
    {
        $getReq = $this->mod->getData('row','*','request_top_up',null,null,null,array('id'=>$id));
        $update = $this->mod->updateData('request_top_up',array(
            'status_topup'  => '2',
        ), array('id'=>$id));

        $app = $this->mod->getData('row','*','app_settings');

        $title = 'Top up Cancel';
        $message = 'Your request has been rejected';
        $topic = get_token($getReq->id_user);

        $this->notif->send_notif_to($title, $message, $topic);

        $subject = 'Request Topup Gagal !';
        $emailmessage = 'Maaf kami tidak dapat memproses Request Topup kamu, silahkan lakukan Request Kembali..<br><br>Salam Hormat , ZingGo Team.';
        $host = $app->smtp_host;
        $port = $app->smtp_port;
        $username = $app->smtp_username;
        $password = $app->smtp_password;
        $from = $app->smtp_from;
        $appname = $app->app_name;
        $secure = $app->smtp_secure;
        $address = $app->app_address;
        $linkgoogle = $app->app_linkgoogle;
        $web = $app->app_website;
        $linkimage = base_url(). 'asset/images/' . $app->app_logo;

        $emailuser = get_email($getReq->id_user);

        $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
        $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);

        if ($update) {
            $this->session->set_flashdata('success', 'Data Reject ! Email notification sended..');
            redirect('req_topup/index');
        }
    }

    public function ulang($id)
    {
        $getReq = $this->mod->getData('row','*','request_top_up',null,null,null,array('id'=>$id));
        $update = $this->mod->updateData('request_top_up',array(
            'status_payment'  => '0',
            'struk_pembayaran' => NULL
        ), array('id'=>$id));

        if ($getReq->struk_pembayaran != '') {
            $gambar = $getReq->struk_pembayaran;
            unlink('images/struk_transfer/' . $gambar);
        }

        $app = $this->mod->getData('row','*','app_settings');

        $subject = 'Konfirmasi Ulang !';
        $emailmessage = 'Maaf kami kesulitan memverifikasi data request kamu, silahkan lakukan konfirmasi kembali..<br><br>Salam Hormat , ZingGo Team.';
        $host = $app->smtp_host;
        $port = $app->smtp_port;
        $username = $app->smtp_username;
        $password = $app->smtp_password;
        $from = $app->smtp_from;
        $appname = $app->app_name;
        $secure = $app->smtp_secure;
        $address = $app->app_address;
        $linkgoogle = $app->app_linkgoogle;
        $web = $app->app_website;
        $linkimage = base_url(). 'asset/images/' . $app->app_logo;

        $emailuser = get_email($getReq->id_user);

        $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
        $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);

        if ($update) {
            $this->session->set_flashdata('success', 'Data Reject ! Email notification sended..');
            redirect('req_topup/index');
        }
    }

    public function detail($id)
    {
        $data = array(
            'titlePage'   => 'Topup Detail Request',
            'app'         => $this->app->getappbyid(),
            'data'        => $this->mod->getData('row','*','request_top_up',null,null,null,array('id'=>$id)),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

		$this->load->view('includes/header', $datasettings);
        $this->load->view('request/detail_topup', $data);
        $this->load->view('includes/footer');
    }
    
    function download($file)
	{
		if ($file == null)
        redirect(base_url());

		$data = 'images/struk_transfer/'.$file;
		force_download($data,NULL);
	}

}
