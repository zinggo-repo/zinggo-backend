<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Appsettings extends CI_Controller
{

    public function  __construct()
    {
        parent::__construct();

        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->library('form_validation');
        $this->load->model('Appsettings_model', 'app');
        $this->load->library('upload');
    }

    public function index()
    {
        $data['appsettings'] = $this->app->getappbyid();

        $datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('appsettings/index', $data);
        $this->load->view('includes/footer');
    }

    public function ubahapp()
    {

        $this->form_validation->set_rules('app_name', 'app_name', 'trim|prep_for_form');
        $this->form_validation->set_rules('app_email', 'app_email', 'trim|prep_for_form');
        $this->form_validation->set_rules('app_website', 'app_website', 'trim|prep_for_form');
        $this->form_validation->set_rules('app_linkgoogle', 'app_linkgoogle', 'trim|prep_for_form');
        $this->form_validation->set_rules('app_currency', 'app_currency', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->app->getappbyid();

            if (@$_FILES['app_logo']['name']) {

                $config['upload_path']      = './asset/images';
                $config['allowed_types']    = 'gif|jpg|png|jpeg';
                $config['max_size']         = '10000';
                $config['file_name']        = 'name';
                $config['encrypt_name']     = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('app_logo')) {
                    $this->session->set_flashdata('hapus', $this->upload->display_errors());
                    redirect('appsettings');                  
                } else {
                    if ($data['app_logo'] != '') {
                        $gambar = $data['app_logo'];
                        unlink('asset/images/' . $gambar);
                    }
                }
                $app_logo = html_escape($this->upload->data('file_name'));
            }else{
                $app_logo = $data['app_logo'];
            }

            if (@$_FILES['app_icon']['name']) {

                $config['upload_path']      = './asset/images';
                $config['allowed_types']    = 'gif|jpg|png|jpeg';
                $config['max_size']         = '10000';
                $config['file_name']        = 'name';
                $config['encrypt_name']     = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('app_icon')) {
                    $this->session->set_flashdata('hapus', $this->upload->display_errors());
                    redirect('appsettings');                  
                } else {
                    if ($data['app_icon'] != '') {
                        $gambar = $data['app_icon'];
                        unlink('asset/images/' . $gambar);
                    }
                }
                $app_icon = html_escape($this->upload->data('file_name'));
            }else{
                $app_icon = $data['app_icon'];
            }

            $data             = [
                'app_logo'                  => $app_logo,
                'app_icon'                  => $app_icon,
                'app_name'                  => html_escape($this->input->post('app_name', TRUE)),
                'app_email'                 => html_escape($this->input->post('app_email', TRUE)),
                'app_website'               => html_escape($this->input->post('app_website', TRUE)),
                'app_privacy_policy'        => $this->input->post('app_privacy_policy', TRUE),
                'app_aboutus'               => $this->input->post('app_aboutus', TRUE),
                'app_address'               => $this->input->post('app_address'),
                'app_linkgoogle'            => html_escape($this->input->post('app_linkgoogle', TRUE)),
                'app_currency'              => html_escape($this->input->post('app_currency', TRUE))
            ];

            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('appsettings/index');
            } else {

                $this->app->ubahdataappsettings($data);
                $this->session->set_flashdata('ubah', 'APP Has Been Change');
                redirect('appsettings');
            }
        } else {

            $data['appsettings'] = $this->app->getappbyid();

            $datasettings['appsettings'] = $this->app->getappbyid();

            $this->load->view('includes/header', $datasettings);
            $this->load->view('appsettings/index', $data);
            $this->load->view('includes/footer');
        }
    }

    public function ubahemail()
    {

        $this->form_validation->set_rules('email_subject', 'email_subject', 'trim|prep_for_form');
        $this->form_validation->set_rules('email_subject_confirm', 'email_subject', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $data             = [
                'email_subject'                    => html_escape($this->input->post('email_subject', TRUE)),
                'email_subject_confirm'                    => html_escape($this->input->post('email_subject_confirm', TRUE)),
                'email_text1'                    => $this->input->post('email_text1'),
                'email_text2'                    => $this->input->post('email_text2'),
                'email_text3'                    => $this->input->post('email_text3'),
                'email_text4'                    => $this->input->post('email_text4')
            ];


            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('appsettings/index');
            } else {

                $this->app->ubahdataemail($data);
                $this->session->set_flashdata('ubah', 'Email Has Been Change');
                redirect('appsettings');
            }
        } else {

            $data['appsettings'] = $this->app->getappbyid();

            $datasettings['appsettings'] = $this->app->getappbyid();

            $this->load->view('includes/header', $datasettings);
            $this->load->view('appsettings/index', $data);
            $this->load->view('includes/footer');
        }
    }

    public function ubahsmtp()
    {

        $this->form_validation->set_rules('smtp_host', 'smtp_host', 'trim|prep_for_form');
        $this->form_validation->set_rules('smtp_port', 'smtp_port', 'trim|prep_for_form');
        $this->form_validation->set_rules('smtp_username', 'smtp_username', 'trim|prep_for_form');
        $this->form_validation->set_rules('smtp_password', 'smtp_password', 'trim|prep_for_form');
        $this->form_validation->set_rules('smtp_form', 'smtp_form', 'trim|prep_for_form');
        $this->form_validation->set_rules('smtp_secure', 'smtp_secure', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $data             = [
                'smtp_host'                        => html_escape($this->input->post('smtp_host', TRUE)),
                'smtp_port'                        => html_escape($this->input->post('smtp_port', TRUE)),
                'smtp_username'                    => html_escape($this->input->post('smtp_username', TRUE)),
                'smtp_password'                    => html_escape($this->input->post('smtp_password', TRUE)),
                'smtp_from'                        => html_escape($this->input->post('smtp_from', TRUE)),
                'smtp_secure'                    => html_escape($this->input->post('smtp_secure', TRUE))
            ];


            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('appsettings/index');
            } else {
                $this->app->ubahdatasmtp($data);
                $this->session->set_flashdata('ubah', 'SMTP Has Been Change');
                redirect('appsettings');
            }
        } else {

            $data['appsettings'] = $this->app->getappbyid();

            $datasettings['appsettings'] = $this->app->getappbyid();

            $this->load->view('includes/header', $datasettings);
            $this->load->view('appsettings/index', $data);
            $this->load->view('includes/footer');
        }
    }

    public function ubahstripe()
    {

        $this->form_validation->set_rules('stripe_secret_key', 'stripe_secret_key', 'trim|prep_for_form');
        $this->form_validation->set_rules('stripe_published_key', 'stripe_published_key', 'trim|prep_for_form');
        $this->form_validation->set_rules('stripe_status', 'stripe_status', 'trim|prep_for_form');
        $this->form_validation->set_rules('stripe_active', 'stripe_active', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $data             = [
                'stripe_secret_key'                    => html_escape($this->input->post('stripe_secret_key', TRUE)),
                'stripe_published_key'                => html_escape($this->input->post('stripe_published_key', TRUE)),
                'stripe_status'                        => html_escape($this->input->post('stripe_status', TRUE)),
                'stripe_active'                        => html_escape($this->input->post('stripe_active', TRUE))
            ];
            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('appsettings/index');
            } else {


                $this->app->ubahdatastripe($data);
                $this->session->set_flashdata('ubah', 'Stripe Has Been Change');
                redirect('appsettings');
            }
        } else {

            $data['appsettings'] = $this->app->getappbyid();

            $datasettings['appsettings'] = $this->app->getappbyid();

            $this->load->view('includes/header', $datasettings);
            $this->load->view('appsettings/index', $data);
            $this->load->view('includes/footer');
        }
    }

    public function ubahpaypal()
    {

        $this->form_validation->set_rules('paypal_key', 'paypal_key', 'trim|prep_for_form');
        $this->form_validation->set_rules('app_currency_text', 'app_currency_text', 'trim|prep_for_form');
        $this->form_validation->set_rules('paypal_mode', 'paypal_mode', 'trim|prep_for_form');
        $this->form_validation->set_rules('paypal_active', 'paypal_active', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $data             = [
                'paypal_key'                    => html_escape($this->input->post('paypal_key', TRUE)),
                'app_currency_text'                => html_escape($this->input->post('app_currency_text', TRUE)),
                'paypal_mode'                        => html_escape($this->input->post('paypal_mode', TRUE)),
                'paypal_active'                        => html_escape($this->input->post('paypal_active', TRUE))
            ];
            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('appsettings/index');
            } else {


                $this->app->ubahdatapaypal($data);
                $this->session->set_flashdata('ubah', 'Paypal Has Been Change');
                redirect('appsettings');
            }
        } else {

            $data['appsettings'] = $this->app->getappbyid();

            $datasettings['appsettings'] = $this->app->getappbyid();

            $this->load->view('includes/header', $datasettings);
            $this->load->view('appsettings/index', $data);
            $this->load->view('includes/footer');
        }
    }
}
