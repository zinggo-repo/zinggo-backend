<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }

        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->library('form_validation');
        $this->load->model('Appsettings_model', 'app');
    }

    public function index()
    {
        $data = array(
            'titlePage'   => 'Admin List',
            'data'        => $this->mod->getData('result','*','admin'),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('admin/index', $data);
        $this->load->view('includes/footer');
    }

    public function tambah()
    {
        $data = array(
            'titlePage'   => 'Add Admin',
            'lokasi'      => base_url('admin/do_tambah'),
            'dtrole'      => $this->mod->getData('result','*','role_access'),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('admin/form', $data);
        $this->load->view('includes/footer');
    }

    public function ubah($id = null)
    {
        $data = array(
            'titlePage'   => 'Edit Admin',
            'lokasi'      => base_url('admin/do_ubah'),
            'dtrole'      => $this->mod->getData('result','*','role_access'),
            'data'        => $this->mod->getData('row','*','admin',null,null,null,array('id'=>$id)),
        );

		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('admin/form', $data);
        $this->load->view('includes/footer');
    }

    public function do_tambah()
    {
        $password = html_escape($this->input->post('pass', TRUE));
        $username = html_escape($this->input->post('username', TRUE));
        $role_id = html_escape($this->input->post('roleid', TRUE));
        $email = html_escape($this->input->post('email', TRUE));

        $this->form_validation->set_rules('username', 'Username', 'trim|prep_for_form');
        $this->form_validation->set_rules('pass', 'Password', 'trim|prep_for_form');
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|is_unique[admin.email]');
        $this->form_validation->set_rules('roleid', 'Role Access', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $config['upload_path']      = './images/admin/';
            $config['allowed_types']    = 'gif|jpg|png|jpeg';
            $config['max_size']         = '10000';
            $config['file_name']        = 'name';
            $config['encrypt_name']     = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('imageadmin')) {

                $foto = html_escape($this->upload->data('file_name'));
            } else {
                $foto = 'noimage.jpg';
            }

            $data             = [

                'user_name'                 => $username,
                'role_id'                   => $role_id,
                'email'                     => $email,
                'image'                     => $foto,
                'password'                  => sha1($password),

            ];
            $insert = $this->mod->insertData('admin',$data);
            if ($insert) {
                $this->session->set_flashdata('success', 'User Has Been Added');
                redirect('admin/tambah');
            }            
        } else {

            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/tambah');
        }
    }

    public function do_ubah()
    {
        $id = $this->input->post('id');
        $password = html_escape($this->input->post('pass', TRUE));
        $username = html_escape($this->input->post('username', TRUE));
        $role_id = html_escape($this->input->post('roleid', TRUE));
        $email = html_escape($this->input->post('email', TRUE));

        $this->form_validation->set_rules('username', 'Username', 'trim|prep_for_form');
        $this->form_validation->set_rules('pass', 'Password', 'trim|prep_for_form');
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form');
        $this->form_validation->set_rules('roleid', 'Role Access', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $data = $this->mod->getData('row','image','admin',null,null,null,array('id'=>$id));

            $config['upload_path']      = './images/admin/';
            $config['allowed_types']    = 'gif|jpg|png|jpeg';
            $config['max_size']         = '10000';
            $config['file_name']        = 'name';
            $config['encrypt_name']     = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('imageadmin')) {
                if ($data->image != 'noimage.jpg') {
                    $gambar = $data->image;
                    unlink('images/admin/' . $gambar);
                }

                $foto = html_escape($this->upload->data('file_name'));
            } else {
                $foto = $data->image;
            }
            
            $data = [
                'user_name'                 => $username,
                'role_id'                   => $role_id,
                'email'                     => $email,
                'image'                     => $foto,
                'password'                  => sha1($password),
            ];

            $update = $this->mod->updateData('admin',$data, array('id'=>$id));
            if ($update) {
                $this->session->set_flashdata('ubah', 'Admin Has Been Updated');
                redirect('admin/ubah/'.$id);
            }

        }else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/ubah/'.$id);
        }
    }

    public function hapus($id)
    {
        $data = $this->mod->getData('row','image','admin',null,null,null,array('id'=>$id));
        $gambar = $data->image;
        unlink('images/admin/' . $gambar);

        $this->mod->deleteData('admin',array('id'=>$id));

        $this->session->set_flashdata('success', 'Admin Has Been Deleted');
        redirect('admin/index');
    }

}
