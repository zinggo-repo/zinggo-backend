<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Infobank extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }

        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Mod_crud', 'mod');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = array(
            'titlePage'   => 'List Bank',
            'data'        => $this->mod->getData('result','*','info_bank'),
        );

        $this->load->model('Appsettings_model', 'app');
		$datasettings['appsettings'] = $this->app->getappbyid();

        $this->load->view('includes/header', $datasettings);
        $this->load->view('infobank/index', $data);
        $this->load->view('includes/footer');
    }

    public function tambah()
    {
        $this->form_validation->set_rules('norek', 'No rek', 'trim|prep_for_form');
        $this->form_validation->set_rules('atasnama', 'Atas Nama', 'trim|prep_for_form');
        $this->form_validation->set_rules('namabank', 'Nama bank', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $data = [
                'no_rek'        => html_escape($this->input->post('norek', TRUE)),
                'atas_nama'     => html_escape($this->input->post('atasnama', TRUE)),
                'nama_bank'     => html_escape($this->input->post('namabank', TRUE)),
            ];

            $this->mod->insertData('info_bank',$data);
            $this->session->set_flashdata('tambah', 'Bank Has Been Added');
            redirect('infobank');
        }
    }

    public function hapus($id)
    {
        $query 	= $this->mod->deleteData('info_bank', array('id_bank' => $id));        
		if ($query){
            $this->session->set_flashdata('hapus', 'Bank Has Been Deleted');
            redirect('infobank');
        }else{
            $this->session->set_flashdata('error', 'An error occurred while deleting data !');
            redirect('infobank');
        }
    }


    public function ubah()
    {


        $this->form_validation->set_rules('norek', 'No rek', 'trim|prep_for_form');
        $this->form_validation->set_rules('atasnama', 'Atas Nama', 'trim|prep_for_form');
        $this->form_validation->set_rules('namabank', 'Nama bank', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $id = $this->input->post('id_bank');
            
            $data = [
                'no_rek'        => html_escape($this->input->post('norek', TRUE)),
                'atas_nama'     => html_escape($this->input->post('atasnama', TRUE)),
                'nama_bank'     => html_escape($this->input->post('namabank', TRUE)),
            ];

            $this->mod->updateData('info_bank',$data, array('id_bank'=>$id));
            $this->session->set_flashdata('ubah', 'Bank Has Been Updated');
            redirect('infobank');
        }
    }
}
