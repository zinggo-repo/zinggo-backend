<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Promosi_slider extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
        $this->load->helper("url","any");
        $this->load->database();
        $this->load->model('Mod_crud', 'mod');
        date_default_timezone_set('Asia/Jakarta');
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    function promosi_byid_get()
    {
        $id = $this->get('id');
        //$resp = array();
        $request_data = $this->mod->getData('result','p.*,f.*','promosi p',null,null,array('fitur f'=>'p.fitur_promosi = f.id_fitur'),array('p.type_promosi'=>'service','p.fitur_promosi'=>$id,'p.is_show'=> 1 ,'tanggal_berakhir >'=>date("Y-m-d")));
        ($request_data == false) ? $request_data = [] : $request_data = $request_data ;
        $message = array(
            'id_fitur' => $id,
            'count' => count($request_data),
            'slider' => $request_data
        );
        $this->response($message, 200);
    }

}