<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Request_tarikdana extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
        $this->load->helper("url","any");
        $this->load->database();
        $this->load->model('Mod_crud', 'mod');
        $this->load->model('Tarikdana_model','top');
        $this->load->model('Email_model');
        date_default_timezone_set('Asia/Jakarta');
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    function allpenarikan_get()
    {
        $penarikan_data = $this->top->get_all_penarikan();
        $message = array(
            'data' => $penarikan_data->result()
        );
        $this->response($message, 200);
    }

    function insertPenarikan_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $condition = array(
            'id_user' => $dec_data->iduser,
        );
        $getsaldo = $this->top->get_saldo($condition);
        
        if ($getsaldo->saldo_lama >= $dec_data->nominal) {
            $code = 'INVW' . date('Ymd') . rand(0, 99999);
            $data_penarikan = array(
                'codetarik' => $code,
                'statustarik' => 0,
                'iduser' => $dec_data->iduser,
                'nominal' => $dec_data->nominal,
                'tgltarik' => date('Y-m-d H:i:s'),
                'norek' => $dec_data->norek, 
                'atasnama' => $dec_data->atasnama, 
                'namabank' => $dec_data->namabank,     
            );
            $insert = $this->top->insertPenarikan($data_penarikan);
            //$insert = true;
            if($insert){
                //$bank = $this->mod->getData('result','*','info_bank');
                $app = $this->mod->getData('row','*','app_settings');

                $subject = 'Request Penarikan Dana Terkirim !';
                $emailmessage = 'Request Penarikan Dana kamu berhasil terkirim, dengan total nominal <strong>Rp.' . number_format($data_penarikan['nominal'],0,",",".") . '</strong> Berikut Code Request Penarikan kamu <strong>' . $data_penarikan['codetarik'] . '</strong> <br><br>Sistem sedang melakukan verifikasi data, Silahkan tunggu 1/24 jam..<br><br>Salam Hormat , ZingGo Team.';
                $host = $app->smtp_host;
                $port = $app->smtp_port;
                $username = $app->smtp_username;
                $password = $app->smtp_password;
                $from = $app->smtp_from;
                $appname = $app->app_name;
                $secure = $app->smtp_secure;
                $address = $app->app_address;
                $linkgoogle = $app->app_linkgoogle;
                $web = $app->app_website;
                $linkimage = base_url(). 'asset/images/' . $app->app_logo;

                $emailuser = get_email($data_penarikan['iduser']);
                //$emailuser = 'masantonpurnama@gmail.com';

                $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
                $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);
                //$send = true;
                if($send){
                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => $data_penarikan
                    );
                    $this->response($message, 200);
                }
            }else{
                $message = array(
                    'code' => '201',
                    'message' => 'failed',
                    'data' => []
                );
                $this->response($message, 201);
            }    
        }else{
            $message = array(
                'code' => '201',
                'message' => 'Maaf saldo anda tidak mencukupi, untuk penarikan dana !',
                'data' => []
            );
            $this->response($message, 201);
        }        

    }

    function tarikdana_byid_get()
    {
        $id = $this->get('iduser');
        $resp = array();
        $request_data = $this->top->get_tarikdana_byid($id);
        foreach ($request_data as $key) {
            
            $result = array(
                'iduser' => $key->id_user,
                'codetarik' => $key->code_penarikan,
                'nominal' => $key->nominal,
                'statustarik' => intval($key->status_penarikan),
                'tgltarik' => $key->tgl_penarikan,
                'norek' => $key->no_rekening,
                'atasnama' => $key->atas_nama,
                'namabank' => $key->nama_bank,
            );

            array_push($resp, $result);
        }
        $message = array(
            'iduser' => $id,
            'count' => count($resp),
            'data' => $resp
        );
        $this->response($message, 200);
    }

}