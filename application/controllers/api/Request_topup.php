<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Request_topup extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ci_ext_model', 'ci_ext');
        $ci_ext = $this->ci_ext->ciext();
        if (!$ci_ext) {
            redirect(gagal);
        }
        $this->load->helper("url","any");
        $this->load->database();
        $this->load->model('Mod_crud', 'mod');
        $this->load->model('RequestTopup_model','top');
        $this->load->model('Email_model');
        date_default_timezone_set('Asia/Jakarta');
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    // tambah request topup
    function addrequest_topup_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);       
        // check request topup user
        if (!$this->top->check_topup_user($dec_data->iduser)) {
            $code = 'INVT' . date('Ymd') . rand(0, 99999);
            $data_request = array(
                'iduser' => $dec_data->iduser,
                'codetopup' => $code,
                'nominal' => $dec_data->nominal,
                'statustopup' => 0,
                'paymentstatus' => 0,
                //'tgl_transfer' => date('Y-m-d H:i:s'),     
            );
            $insert = $this->top->insertRequest_topup($data_request);
            //$insert = true;

            if($insert){
                $bank = $this->mod->getData('result','*','info_bank');
                $app = $this->mod->getData('row','*','app_settings');
                
                foreach ($bank as $key) {
                    $infobank[] = 'No rekening. ' . $key->no_rek . '<br>A/N. ' . $key->atas_nama . '<br>BANK ' . $key->nama_bank . '<br><br>';
                }

                $subject = 'Request Topup Terkirim !';
                $emailmessage = 'Request Topup kamu berhasil terkirim, dengan total nominal <strong>Rp.' . number_format($data_request['nominal'],0,",",".") . '</strong> Berikut Code Request Topup kamu <strong>' . $data_request['codetopup'] . '</strong> <br><br>Silahkan lakukan Transfer pada rekening berikut: <br><br>' . $infobank[0] . $infobank[1] . $infobank[2] . 'Salam Hormat , ZingGo Team.';
                $host = $app->smtp_host;
                $port = $app->smtp_port;
                $username = $app->smtp_username;
                $password = $app->smtp_password;
                $from = $app->smtp_from;
                $appname = $app->app_name;
                $secure = $app->smtp_secure;
                $address = $app->app_address;
                $linkgoogle = $app->app_linkgoogle;
                $web = $app->app_website;
                $linkimage = base_url(). 'asset/images/' . $app->app_logo;

                $emailuser = get_email($data_request['iduser']);
                //$emailuser = 'masantonpurnama@gmail.com';

                $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
                $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);
                //$send = true;
                if($send){
                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => $data_request
                    );
                    $this->response($message, 200);
                }
            }else{
                $message = array(
                    'code' => '201',
                    'message' => 'failed',
                    'data' => []
                );
                $this->response($message, 201);
            }

        }else{
            $message = array(
                'code' => '201',
                'message' => 'Maaf request anda dalam antrian !',
                'data' => []
            );
            $this->response($message, 201);
        }
        
    }

    // get code topup
    function getcode_topup_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);

        $condition = array(
            'id_user' => $decoded_data->iduser,
            'status_topup'=> 0
        );

        $request_data = $this->top->get_code($condition);
        foreach ($request_data as $key) {
            $result = array(
                'iduser' => $key->id_user,
                'codetopup' => $key->code_request_top_up,
                'nominal' => $key->nominal,
                'statustopup' => intval($key->status_topup),
                'paymentstatus' => intval($key->payment_status)
            );
        }
        $message = array(
            'data' => $result
        );
        $this->response($message, 200);
    }

    // get all request topup
    function allrequest_topup_get()
    {
        $resp = array();
        $request_data = $this->top->get_allrequest_topup();
        foreach ($request_data as $key) {
            
            $result = array(
                'iduser' => $key->id_user,
                'codetopup' => $key->code_request_top_up,
                'nominal' => $key->nominal,
                'statustopup' => intval($key->status_topup),
                'paymentstatus' => intval($key->payment_status),
                'tgltransfer' => $key->tgl_transfer,
                'norek' => $key->no_rekening,
                'atasnama' => $key->atas_nama,
                'namabank' => $key->nama_bank,
            );

            array_push($resp, $result);
        }
        $message = array(
            'data' => $resp
        );
        $this->response($message, 200);
    }

    // Topup per id
    function topup_byid_get()
    {
        $id = $this->get('iduser');
        $resp = array();
        $request_data = $this->top->get_topup_byid($id);
        foreach ($request_data as $key) {
            
            $result = array(
                'iduser' => $key->id_user,
                'codetopup' => $key->code_request_top_up,
                'nominal' => $key->nominal,
                'statustopup' => intval($key->status_topup),
                'paymentstatus' => intval($key->payment_status),
                'struk' => $key->struk_pembayaran,
                'tgltransfer' => $key->tgl_transfer,
                'norek' => $key->no_rekening,
                'atasnama' => $key->atas_nama,
                'namabank' => $key->nama_bank,
            );

            array_push($resp, $result);
        }
        $message = array(
            'iduser' => $id,
            'count' => count($resp),
            'data' => $resp
        );
        $this->response($message, 200);
    }

    // get info bank
    function info_bank_get()
    {
        $resp = array();
        $request_data = $this->top->get_allbank();
        foreach ($request_data as $key) {
            
            $result = array(
                'idbank' => $key->id_bank,
                'norek' => $key->no_rek,
                'atasnama' => $key->atas_nama,
                'namabank' => $key->nama_bank,
            );

            array_push($resp, $result);
        }
        $message = array(
            'data' => $resp
        );
        $this->response($message, 200);
    }

    // update request topup
    function updaterequest_topup_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        // check request code
        $cek_code = $this->top->check_code($dec_data->code_topup);
        if ($cek_code == true) {

            $image = $dec_data->struk;
            $namafoto = time() . '-' . rand(0, 99999) . ".jpg";
            $path = "images/struk_transfer/" . $namafoto;
            file_put_contents($path, base64_decode($image));
            $data_request = array(
                'iduser' => $dec_data->iduser,
                'nominal' => $dec_data->nominal,
                'tgltransfer' => date('Y-m-d H:i:s'),
                'struk' =>$namafoto,
                'paymentstatus' => 1,
                'norek' => $dec_data->norek,  
                'atasnama' => $dec_data->atasnama,  
                'namabank' => $dec_data->namabank,     
            );
            $insert = $this->top->updateRequest_topup($data_request, $dec_data->code_topup);
            //$insert = true;
            if($insert){
                //$bank = $this->mod->getData('result','*','info_bank');
                $app = $this->mod->getData('row','*','app_settings');

                $subject = 'Konfirmasi Bukti Transfer !';
                $emailmessage = 'Konfirmasi bukti transfer berhasil terkirim, Staff kami sedang memverifikasi silahkan tunggu 1/24 jam<br><br>Salam Hormat , ZingGo Team.';
                $host = $app->smtp_host;
                $port = $app->smtp_port;
                $username = $app->smtp_username;
                $password = $app->smtp_password;
                $from = $app->smtp_from;
                $appname = $app->app_name;
                $secure = $app->smtp_secure;
                $address = $app->app_address;
                $linkgoogle = $app->app_linkgoogle;
                $web = $app->app_website;
                $linkimage = base_url(). 'asset/images/' . $app->app_logo;

                $emailuser = get_email($data_request['iduser']);
                //$emailuser = 'masantonpurnama@gmail.com';

                $content = $this->Email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $linkimage, $web);
                $send = $this->Email_model->emailsend($subject, $emailuser, $content, $host, $port, $username, $password, $from, $appname, $secure);

                if($send){
                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => $data_request
                    );
                    $this->response($message, 200);
                }
            }else{
                $message = array(
                    'code' => '201',
                    'message' => 'failed',
                    'data' => []
                );
                $this->response($message, 201);
            } 
        }else{
            $message = array(
                'code' => '201',
                'message' => 'Code topup not found!',
                'data' => []
            );
            $this->response($message, 201);
        }
        
    }
}