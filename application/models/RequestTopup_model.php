<?php

class RequestTopup_model extends CI_model
{
    public function insertRequest_topup($data)
	{	
        $data_insert = array(
            'id_user' => $data['iduser'],
            'code_request_top_up' => $data['codetopup'],
            'nominal' => $data['nominal'],
            'status_topup' => $data['statustopup'],
            'payment_status' => $data['paymentstatus']
        );
		$this->db->insert('request_top_up', $data_insert);
		return true;
    }

    public function updateRequest_topup($data, $code)
	{
        $update_data = array(
            'id_user' => $data['iduser'],
            'nominal' => $data['nominal'],
            'tgl_transfer' => $data['tgltransfer'],
            'struk_pembayaran' =>$data['struk'],
            'payment_status' => $data['paymentstatus'],
            'no_rekening' => $data['norek'],  
            'atas_nama' => $data['atasnama'],  
            'nama_bank' => $data['namabank'],
        );
		$this->db->update('request_top_up', $update_data, array('code_request_top_up'=>$code));
		return true;
	}
    
	public function get_allRequest_topup()
    {
        return  $this->db->get('request_top_up')->result();
    }
    
    public function get_topup_byid($id)
    {
        return  $this->db->where('id_user',$id)->get('request_top_up')->result();
	}

	public function get_allbank()
    {
        return  $this->db->get('info_bank')->result();
	}
	
	public function check_code($code)
    {
        $cek = $this->db->query("SELECT code_request_top_up FROM request_top_up where code_request_top_up = '$code'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
	}
	
	public function check_topup_user($id_user)
    {
        $cek = $this->db->query("SELECT * FROM request_top_up where id_user = '$id_user' AND status_topup = '0'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

	public function get_code($condition)
    {
		$this->db->select('*');
		$this->db->from('request_top_up');
		$this->db->where($condition);
        return $this->db->get()->result();
	}
}