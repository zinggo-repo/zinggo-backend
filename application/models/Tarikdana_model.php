<?php

class Tarikdana_model extends CI_model
{
    public function insertPenarikan($data = null)
	{
		$insert_data = array(
			'code_penarikan' => $data['codetarik'],
            'status_penarikan' => $data['statustarik'],
            'id_user' => $data['iduser'],
            'nominal' => $data['nominal'],
            'tgl_penarikan' => $data['tgltarik'],
            'no_rekening' => $data['norek'], 
            'atas_nama' => $data['atasnama'], 
            'nama_bank' => $data['namabank'],
		);
		$this->db->insert('request_penarikan', $insert_data);
		return true;
    }
    
	public function get_all_penarikan()
    {
		return  $this->db->get('request_penarikan')->result();
	}

	public function get_tarikdana_byid($id)
    {
        return  $this->db->where('id_user',$id)->get('request_penarikan')->result();
	}
	
	public function get_saldo($condition)
    {
		$this->db->select('saldo as saldo_lama');
		$this->db->from('saldo');
		$this->db->where($condition);
        return $this->db->get()->row();
	}
}