<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* @Function_name : show_level
* @Return_type : String
* @Author : Anton Purnama/082118115288
*/	
if ( ! function_exists('show_level'))
{
	function show_level($level)
	{
		$currentLevel = $_SESSION['roleid'];
		
		if (in_array($currentLevel, $level)) {
		    return "";
		}else{
			return "display: none;";
		}	
	}
}

if (!function_exists('get_user_name'))
{
	function get_user_name($id_user = null)
	{
		$CI =& get_instance();

        $inisial = substr($id_user,0,1);
        if ($inisial == 'P') {
            $getData = $CI->mod->getData('row','fullnama','pelanggan',null,null,null,array('id'=>$id_user));
            $nama_user = $getData->fullnama;
        }elseif ($inisial == 'M') {
            $getData = $CI->mod->getData('row','nama_mitra','mitra',null,null,null,array('id_mitra'=>$id_user));
            $nama_user = $getData->nama_mitra;
        }else {
            $getData = $CI->mod->getData('row','nama_driver','driver',null,null,null,array('id'=>$id_user));
            $nama_user = $getData->nama_driver;
        }
		if ($getData) :
			return $nama_user;
		else :
			return false;
		endif;
	}
}

if (!function_exists('get_token'))
{
	function get_token($id_user = null)
	{
		$CI =& get_instance();

        $inisial = substr($id_user,0,1);
        if ($inisial == 'P') {
            $getData = $CI->mod->getData('row','token','pelanggan',null,null,null,array('id'=>$id_user));
            $token = $getData->token;
        }elseif ($inisial == 'M') {
			$getMitra = $CI->mod->getData('row','id_merchant','mitra',null,null,null,array('id_mitra'=>$id_user));
			$getData = $CI->mod->getData('row','token_merchant','merchant',null,null,null,array('id_merchant'=>$getMitra->id_merchant));
            $token = $getData->token_merchant;
        }else {
            $getData = $CI->mod->getData('row','reg_id','driver',null,null,null,array('id'=>$id_user));
            $token = $getData->reg_id;
        }
		if ($getData) :
			return $token;
		else :
			return false;
		endif;
	}
}

if (!function_exists('get_email'))
{
	function get_email($id_user = null)
	{
		$CI =& get_instance();

        $inisial = substr($id_user,0,1);
        if ($inisial == 'P') {
            $getData = $CI->mod->getData('row','email','pelanggan',null,null,null,array('id'=>$id_user));
            $email_user = $getData->email;
        }elseif ($inisial == 'M') {
            $getData = $CI->mod->getData('row','email_mitra','mitra',null,null,null,array('id_mitra'=>$id_user));
            $email_user = $getData->email_mitra;
        }else {
            $getData = $CI->mod->getData('row','email','driver',null,null,null,array('id'=>$id_user));
            $email_user = $getData->email;
        }
		if ($getData) :
			return $email_user;
		else :
			return false;
		endif;
	}
}

if (!function_exists('role_name'))
{
	function role_name($id = null)
	{
		$CI =& get_instance();

		$getData = $CI->mod->getData('row','role','role_access',null,null,null,array('role_id'=>$id));
		
		if ($getData) :
			return $getData->role;
		else :
			return false;
		endif;
	}
}

if (!function_exists('status'))
{
	function status($id = null)
	{
		$CI =& get_instance();

		$getData = $CI->mod->getData('row','status','stats',null,null,null,array('id'=>$id));
		
		if ($getData) :
			return $getData->status;
		else :
			return false;
		endif;
	}
}

if (!function_exists('totalReq'))
{
	function totalReq()
	{
		$CI =& get_instance();

        $countTop = $CI->mod->getCountBy('request_top_up',array('status_topup'=>'0'));
        $countTar = $CI->mod->getCountBy('request_penarikan',array('status_penarikan'=>'0'));
        $totalCount = $countTop + $countTar;

		if ($totalCount) :
			return $totalCount;
		else :
			return false;
		endif;
	}
}

if (!function_exists('totalTop'))
{
	function totalTop()
	{
		$CI =& get_instance();

        $countTop = $CI->mod->getCountBy('request_top_up',array('status_topup'=>'0'));

		if ($countTop) :
			return $countTop;
		else :
			return false;
		endif;
	}
}

if (!function_exists('totalTar'))
{
	function totalTar()
	{
		$CI =& get_instance();

        $countTar = $CI->mod->getCountBy('request_penarikan',array('status_penarikan'=>'0'));

		if ($countTar) :
			return $countTar;
		else :
			return false;
		endif;
	}
}

if (!function_exists('check_struk'))
{
	function check_struk($id)
	{
		$CI =& get_instance();

        $cekStruk = $CI->mod->checkData('struk_pembayaran','request_top_up',array('struk_pembayaran = ""','id = "'.$id.'"'));

		if ($cekStruk) :
			return 'disabled';
		else :
			return false;
		endif;
	}
}
