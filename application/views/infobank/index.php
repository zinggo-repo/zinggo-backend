<!-- partial -->
<div class="content-wrapper">
    <div class="card">
        <div class="card-body">
            <?php if ($this->session->flashdata('error') or $this->session->flashdata('hapus')) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                    <?php echo $this->session->flashdata('hapus'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('ubah') or $this->session->flashdata('tambah')) : ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('ubah'); ?>
                    <?php echo $this->session->flashdata('tambah'); ?>
                </div>
            <?php endif; ?>
            <div>
                <button class="btn btn-info" id="tombolAdd"><i class="mdi mdi-plus-circle-outline"></i>Add Bank</button>
            </div>
            <br>
            <div id="tempatData"></div>

            <div id="elementform" style="display:none;">
                <h4 class="card-title">edit bank</h4>
                <br>
                <?= form_open_multipart('infobank/ubah'); ?>
                <input type="hidden" class="form-control" id="valid" name="id_bank" style="width:60%" value="" required>
                <div class="form-group">
                    <label for="norek">No. Rek</label>
                    <input type="text" class="form-control" id="norek" name="norek" style="width:60%" value="" required>
                </div>
                <div class="form-group">
                    <label for="atasnama">Atas Nama</label>
                    <input type="text" class="form-control" id="atasnama" name="atasnama" style="width:60%" value="" required>
                </div>
                <div class="form-group">
                    <label for="namabank">Nama Bank</label>
                    <input type="text" class="form-control" id="namabank" name="namabank" style="width:60%" value="" required>
                </div>
                <div class="row">
                    <div class="col-7 text-right">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <?= form_close(); ?>
                        <span onclick="balikan()" class="btn btn-light">Cancel</span>
                    </div>
                </div>
                <br>
            </div>

            <h4 class="card-title"><?=$titlePage?></h4>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <h1 id="jumlah" style="display: none;"><?= count($data) ?></h1>
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No. Rek</th>
                                    <th>Atas Nama</th>
                                    <th>Nama Bank</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($data as $key) { ?>
                                    <tr>
                                        <h1 id="idbank<?= $i ?>" style="display:none;"><?= $key->id_bank ?></h1>
                                        <h1 id="norek<?= $i ?>" style="display:none;"><?= $key->no_rek ?></h1>
                                        <h1 id="atasnama<?= $i ?>" style="display:none;"><?= $key->atas_nama ?></h1>
                                        <h1 id="namabank<?= $i ?>" style="display:none;"><?= $key->nama_bank ?></h1>
                                        <td><?= $i ?></td>
                                        <td id="norek<?= $i ?>"><?= $key->no_rek ?></td>
                                        <td id="atasnama<?= $i ?>"><?= $key->atas_nama ?></td>
                                        <td id="namabank<?= $i ?>"><?= $key->nama_bank ?></td>
                                        <td>
                                            <button onclick="update(<?= $i ?>);" class="btn btn-outline-success">Edit</button>
                                            <a href="<?= base_url(); ?>infobank/hapus/<?= $key->id_bank ?>" onclick="return confirm ('are you sure Delete this bank?')">
                                                <button class="btn btn-outline-danger">Delete</button></a>
                                        </td>
                                    </tr>

                                <?php $i++;
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->


<script>
    const tombol = document.getElementById('tombolAdd');
    tombol.addEventListener("click", function() {
        const isi = document.getElementById('tempatData');
        isi.innerHTML = formAdd();
        const kembali = document.getElementById('cancel');
        kembali.addEventListener("click", function() {
            isi.innerHTML = backlah();
        })
    });

    function backlah() {
        return ``
    }

    function formAdd() {
        return `<h4 class="card-title">Add Bank</h4>
                <br>
                <?= form_open_multipart('infobank/tambah'); ?>
                <div class="form-group">
                    <label for="norek">No. Rek</label>
                    <input type="text" class="form-control" id="norek" name="norek" style="width:60%" value="" required>
                </div>
                <div class="form-group">
                    <label for="atasnama">Atas Nama</label>
                    <input type="text" class="form-control" id="atasnama" name="atasnama" style="width:60%" value="" required>
                </div>
                <div class="form-group">
                    <label for="namabank">Nama Bank</label>
                    <input type="text" class="form-control" id="namabank" name="namabank" style="width:60%" value="" required>
                </div>
                <div class="row">
                    <div class="col-7 text-right">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <?= form_close(); ?>
                        <button id="cancel" class="btn btn-light">Cancel</button>
                    </div>
                </div>
                <br>`
    }
    const jumlah = document.getElementById("jumlah").innerHTML

    for (let i = 0; i < jumlah; i++) {

        function update(i) {
            let idbank = document.getElementById(`idbank${i}`).innerHTML
            let norek = document.getElementById(`norek${i}`).innerHTML
            let atasnama = document.getElementById(`atasnama${i}`).innerHTML
            let namabank = document.getElementById(`namabank${i}`).innerHTML
            
            let editdoc = document.getElementById('elementform');
            editdoc.style = "display:block;";
            document.getElementById(`norek`).value = norek;
            document.getElementById(`atasnama`).value = atasnama;
            document.getElementById(`namabank`).value = namabank;
            document.getElementById(`valid`).value = idbank;

        }



    }

    function balikan() {
        document.getElementById('elementform').style = "display:none;";
    }
</script>