<!-- partial -->
<div class="content-wrapper">
    <div class="card">
        <div class="card-body">
            <?php if ($this->session->flashdata('error') or $this->session->flashdata('hapus')) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                    <?php echo $this->session->flashdata('hapus'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('ubah') or $this->session->flashdata('tambah')) : ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('ubah'); ?>
                    <?php echo $this->session->flashdata('tambah'); ?>
                </div>
            <?php endif; ?>
            <div>
                <button class="btn btn-info" id="tombolAdd"><i class="mdi mdi-plus-circle-outline"></i>Add Category</button>
            </div>
            <br>
            <div id="tempatData"></div>

            <div id="elementform" style="display:none;">
                <h4 class="card-title">edit category</h4>
                <br>
                <?= form_open_multipart('catgrocery/ubah'); ?>
                <input type="hidden" class="form-control" id="valid" name="idcat" style="width:60%" value="" required>
                <div class="form-group">
                    <label for="catname">Category Name</label>
                    <input type="text" class="form-control" id="catname" name="catname" style="width:60%" value="" required>
                </div>
                <div class="row">
                    <div class="col-7 text-right">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <?= form_close(); ?>
                        <span onclick="balikan()" class="btn btn-light">Cancel</span>
                    </div>
                </div>
                <br>
            </div>

            <h4 class="card-title"><?=$titlePage?></h4>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <h1 id="jumlah" style="display: none;"><?= count($data) ?></h1>
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Category Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($data as $key) { ?>
                                    <tr>
                                        <h1 id="idcat<?= $i ?>" style="display:none;"><?= $key->id_cat_grocery ?></h1>
                                        <h1 id="catname<?= $i ?>" style="display:none;"><?= $key->cat_grocery_name ?></h1>
                                        <td><?= $i ?></td>
                                        <td id="catname<?= $i ?>"><?= $key->cat_grocery_name ?></td>
                                        <td>
                                            <button onclick="update(<?= $i ?>);" class="btn btn-outline-success">Edit</button>
                                            <a href="<?= base_url(); ?>catgrocery/hapus/<?= $key->id_cat_grocery ?>" onclick="return confirm ('are you sure Delete this category?')">
                                                <button class="btn btn-outline-danger">Delete</button></a>
                                        </td>
                                    </tr>

                                <?php $i++;
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->


<script>
    const tombol = document.getElementById('tombolAdd');
    tombol.addEventListener("click", function() {
        const isi = document.getElementById('tempatData');
        isi.innerHTML = formAdd();
        const kembali = document.getElementById('cancel');
        kembali.addEventListener("click", function() {
            isi.innerHTML = backlah();
        })
    });

    function backlah() {
        return ``
    }

    function formAdd() {
        return `<h4 class="card-title">Add Category</h4>
                <br>
                <?= form_open_multipart('catgrocery/tambah'); ?>
                <div class="form-group">
                    <label for="catname">Category Name</label>
                    <input type="text" class="form-control" id="catname" name="catname" style="width:60%" value="" required>
                </div>
                <div class="row">
                    <div class="col-7 text-right">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <?= form_close(); ?>
                        <button id="cancel" class="btn btn-light">Cancel</button>
                    </div>
                </div>
                <br>`
    }
    const jumlah = document.getElementById("jumlah").innerHTML

    for (let i = 0; i < jumlah; i++) {

        function update(i) {
            let idcat = document.getElementById(`idcat${i}`).innerHTML
            let catname = document.getElementById(`catname${i}`).innerHTML
            
            let editdoc = document.getElementById('elementform');
            editdoc.style = "display:block;";
            document.getElementById(`catname`).value = catname;
            document.getElementById(`valid`).value = idcat;

        }



    }

    function balikan() {
        document.getElementById('elementform').style = "display:none;";
    }
</script>