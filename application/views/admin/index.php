<!-- partial -->
<div class="content-wrapper">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div>
                    <a class="btn btn-info" href="<?= base_url(); ?>admin/tambah">
                        <i class="mdi mdi-plus-circle-outline"></i>Add Admin</a>
                </div>
                <br>
                <?php if ($this->session->flashdata('error')) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <h4 class="card-title"><?=$titlePage?></h4>
                <!-- all users -->
                <div class="tab-pane fade show active" id="allusers-2-1" role="tabpanel" aria-labelledby="tab-2-1">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table id="order-listing" class="table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Profile Pic</th>
                                                    <th>Username</th>
                                                    <th>Email</th>
                                                    <th>Role</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1;
                                                foreach ($data as $us) { ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td>
                                                            <img src="<?= base_url('images/admin/') . $us->image; ?>">
                                                        </td>
                                                        <td><?= $us->user_name ?></td>
                                                        <td><?= $us->email ?></td>
                                                        <td><?= role_name($us->role_id) ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>admin/ubah/<?= $us->id?>">
                                                                <button class="btn btn-outline-primary mr-2">Edit</button>
                                                            </a>
                                                            <a href="<?= base_url(); ?>admin/hapus/<?= $us->id?>">
                                                                <button onclick="return confirm ('Are You Sure?')" class="btn btn-outline-danger text-red mr-2">Delete</button>
                                                            </a>
                                                        </td>
                                                    <?php $i++;
                                                } ?>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of all users -->
            </div>
        </div>
    </div>

</div>
<!-- content-wrapper ends -->