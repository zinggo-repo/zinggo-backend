<!-- partial -->
<div class="content-wrapper">
    <div class="row ">
        <div class="col-md-8 offset-md-2 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?=$titlePage?></h4>
                    <?php if ($this->session->flashdata('error')) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('success')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?= form_open_multipart($lokasi); ?>
                    <input type="hidden" class="form-control" id="id" name="id" style="width:60%" value="<?= (isset($data->id)) ? $data->id : '' ;?>" required>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="imageadmin" id="imageadmin" class="dropify" data-max-file-size="3mb" data-default-file="<?= (isset($data->image)) ? base_url('images/admin/') . $data->image : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="<?= (isset($data->user_name)) ? $data->user_name : '' ;?>" placeholder="enter username" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?= (isset($data->email)) ? $data->email : '' ;?>" placeholder="enter email " required>
                    </div>
                    <div class="form-group">
                        <label for="role">Role Access</label>
                        <select class="js-example-basic-single" id="role" name="roleid" style="width:100%">
                            <?php foreach ($dtrole as $ft) { ?>
                                <option <?= ($data->role_id == $ft->role_id) ? 'selected' : '' ;?> value="<?= $ft->role_id ?>"><?= $ft->role ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="enter password" required>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="<?=base_url('admin')?>" class="btn btn-light">Back</a>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of content wrapper -->