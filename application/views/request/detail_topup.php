<!-- partial -->
<div class="content-wrapper">
    <div class="row ">
        <div class="col-md-8 offset-md-2 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?=$titlePage?></h4>
                    <?php if ($this->session->flashdata() or validation_errors()) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= validation_errors() ?>
                            <?php echo $this->session->flashdata('invalid'); ?>
                            <?php echo $this->session->flashdata('demo'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="form-group">
                        <label for="code">Code Request Top Up</label>
                        <input type="text" class="form-control" id="code" name="code" placeholder="enter name" value="<?=$data->code_request_top_up?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="id">ID User</label>
                        <input type="text" class="form-control" id="id" name="id" placeholder="enter name" value="<?=$data->id_user?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="nama">User Name</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="enter name" value="<?=get_user_name($data->id_user)?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="nominal">Topup Nominal</label>
                        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="enter name" value="<?=$app['app_currency'].' '.number_format($data->nominal,0,",",".")?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="rekno">Rek. No</label>
                        <input type="text" class="form-control" id="rekno" name="rekno" placeholder="enter name" value="<?=$data->no_rekening?>" readonly required>
                    </div>
                    <div class="form-group">
                        <label for="an">A/N</label>
                        <input type="text" class="form-control" id="an" name="an" placeholder="enter name" value="<?=$data->atas_nama?>" readonly required>
                    </div>
                    <div class="form-group">
                        <label for="bank">Bank Name</label>
                        <input type="text" class="form-control" id="bank" name="bank" placeholder="enter name" value="<?=$data->nama_bank?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="bank">Status Topup</label><br>
                        <?php if ($data->status_topup == 0) { ?>
                            <label class="badge badge-dark">Pending</label>
                        <?php } else if($data->status_topup == 1) { ?>
                            <label class="badge badge-success">Success</label>
                        <?php } else if($data->status_topup == 2) {?>
                            <label class="badge badge-danger">Rejected</label>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label for="img">Struk Image</label>
                        <input type="text" class="form-control" id="img" name="img" placeholder="enter name" value="<?=(!empty($data->struk_pembayaran)) ? $data->struk_pembayaran : 'Belum Melakukan Konfirmasi Pembayaran'?>" readonly required>
                        <?php if($data->payment_status != 0){?>
                        <br>
                        <img id="myImg" src="<?= base_url('images/struk_transfer/'.$data->struk_pembayaran); ?>" alt="<?=$data->code_request_top_up?>" style="width:100%;max-width:100px">
                            <br>
                        <a style="margin-top:100px;" href="<?= base_url(); ?>req_topup/download/<?= $data->struk_pembayaran ?>">
                            <button class="btn btn-outline-primary text-red mt-2 mb-5">Download</button>
                        </a>
                        <?php } ?>
                    </div>

                    <?php if ($data->status_topup == 0) { ?>
                        <?php if ($data->payment_status != 0) { ?>
                        <a href="<?= base_url(); ?>req_topup/uprove/<?= $data->id ?>">
                            <button class="btn btn-outline-success text-red mr-2">Uprove</button>
                        </a>
                        <?php }?>
                        <a href="<?= base_url(); ?>req_topup/reject/<?= $data->id ?>">
                            <button class="btn btn-outline-danger text-red mr-2">Reject</button>
                        </a>
                    <?php }?>
                    <a href="<?= base_url(); ?>req_topup">
                        <button class="btn btn-light">Back</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<img id="myImg" src="img_snow.jpg" alt="Snow" style="width:100%;max-width:100px">

<!-- The Modal -->
<div id="myModal" class="mymodal">
  <span class="close">&times;</span>
  <img class="mymodal-content" id="img01">
  <div id="caption"></div>
</div>