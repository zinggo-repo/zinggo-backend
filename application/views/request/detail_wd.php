<!-- partial -->
<div class="content-wrapper">
    <div class="row ">
        <div class="col-md-8 offset-md-2 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?=$titlePage?></h4>
                    <?php if ($this->session->flashdata() or validation_errors()) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= validation_errors() ?>
                            <?php echo $this->session->flashdata('invalid'); ?>
                            <?php echo $this->session->flashdata('demo'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="form-group">
                        <label for="code">Code Withdraw</label>
                        <input type="text" class="form-control" id="code" name="code" placeholder="enter name" value="<?=$data->code_penarikan?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="id">ID User</label>
                        <input type="text" class="form-control" id="id" name="id" placeholder="enter name" value="<?=$data->id_user?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="nama">User Name</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="enter name" value="<?=get_user_name($data->id_user)?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="nominal">Withdraw Nominal</label>
                        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="enter name" value="<?=$app['app_currency'].' '.number_format($data->nominal,0,",",".")?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="rekno">Rek. No</label>
                        <input type="text" class="form-control" id="rekno" name="rekno" placeholder="enter name" value="<?=$data->no_rekening?>" readonly required>
                    </div>
                    <div class="form-group">
                        <label for="an">A/N</label>
                        <input type="text" class="form-control" id="an" name="an" placeholder="enter name" value="<?=$data->atas_nama?>" readonly required>
                    </div>
                    <div class="form-group">
                        <label for="bank">Bank Name</label>
                        <input type="text" class="form-control" id="bank" name="bank" placeholder="enter name" value="<?=$data->nama_bank?>" readonly required>
                    </div>

                    <div class="form-group">
                        <label for="bank">Status Penarikan</label><br>
                        <?php if ($data->status_penarikan == 0) { ?>
                            <label class="badge badge-dark">Pending</label>
                        <?php } else if($data->status_penarikan == 1) { ?>
                            <label class="badge badge-success">Success</label>
                        <?php } else if($data->status_penarikan == 2) {?>
                            <label class="badge badge-danger">Rejected</label>
                        <?php } ?>
                    </div>

                    <?php if ($data->status_penarikan == 0) { ?>
                        <a href="<?= base_url(); ?>req_wd/uprove/<?= $data->id ?>">
                            <button class="btn btn-outline-success text-red mr-2">Uprove</button>
                        </a>
                        <a href="<?= base_url(); ?>req_wd/reject/<?= $data->id ?>">
                            <button class="btn btn-outline-danger text-red mr-2">Reject</button>
                        </a>
                    <?php }?>
                    <a href="<?= base_url(); ?>req_wd">
                        <button class="btn btn-light">Back</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>