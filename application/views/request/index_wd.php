<!-- partial -->
<div class="content-wrapper">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <?php if ($this->session->flashdata('success') or $this->session->flashdata('hapus')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <h4 class="card-title"><?=$titlePage?></h4>
                <div class="tab-minimal tab-minimal-success">
                    <div class="tab-content">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Request Withdraw list</h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table id="order-listing" class="table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Date</th>
                                                        <th>Withdraw Code</th>
                                                        <th>ID User</th>
                                                        <th>Full Name</th>
                                                        <th>Nominal</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;
                                                    foreach ($data as $dt) { ?>
                                                        <tr>
                                                            <td><?= $i ?></td>
                                                            <td><?= $dt->tgl_penarikan ?></td>
                                                            <td><?= $dt->code_penarikan ?></td>
                                                            <td><?= $dt->id_user ?></td>
                                                            <td><?=get_user_name($dt->id_user)?></td>
                                                            <td><?= $cr['app_currency'].' '.number_format($dt->nominal, 0, ",", ".") ?></td>
                                                            <td>
                                                                <?php if ($dt->status_penarikan == 0) { ?>
                                                                    <label class="badge badge-dark">Pending</label>
                                                                <?php } else if($dt->status_penarikan == 1) { ?>
                                                                    <label class="badge badge-success">Success</label>
                                                                <?php } else if($dt->status_penarikan == 2) {?>
                                                                    <label class="badge badge-danger">Rejected</label>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <a href="<?= base_url(); ?>req_wd/detail/<?= $dt->id ?>">
                                                                    <button class="btn btn-outline-primary mr-2">View</button>
                                                                </a>
                                                                <?php if ($dt->status_penarikan == 0) { ?>
                                                                    <a href="<?= base_url(); ?>req_wd/uprove/<?= $dt->id ?>">
                                                                        <button class="btn btn-outline-success text-red mr-2">Uprove</button>
                                                                    </a>
                                                                    <a href="<?= base_url(); ?>req_wd/reject/<?= $dt->id ?>">
                                                                        <button class="btn btn-outline-danger text-red mr-2">Reject</button>
                                                                    </a>
                                                                <?php }?>
                                                            </td>
                                                        <?php $i++;
                                                    } ?>
                                                        </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of all users -->

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- content-wrapper ends -->