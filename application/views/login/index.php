<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Zinggo Admin Panel</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?= base_url(); ?>asset/node_modules/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/node_modules/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/node_modules/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?= base_url('asset/images/'.$appsettings['app_icon']); ?>" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper">
            <div class="row">
                <div class="content-wrapper full-page-wrapper d-flex align-items-center auth">
                <!-- <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg"> -->
                    <div class="row w-100">
                        <div class="col-lg-4 mx-auto">
                            <div class="auth-form-light text-left p-5">
                                <?php if ($this->session->flashdata()) : ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php endif; ?>
                                <center><img style="width:200px;height:80px;margin-bottom:30px;" src="<?= base_url('asset/images/'.$appsettings['app_logo']); ?>" alt=""></center>

                                <h4 class="font-weight-light">Hello! Get ready for today</h4>
                                 <!-- <img style="width:60px;height:60px;margin-bottom:30px;" class="center" href="<?= base_url('asset/images/'.$appsettings['app_logo']); ?>" alt="Card image cap"> -->

                                <?= form_open_multipart('login/aksi_login'); ?>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Username" name="user_name" required>
                                    <i class="mdi mdi-account"></i>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
                                    <i class="mdi mdi-eye" onclick="myFunction()"></i>
                                </div>
                                <div class="form-group">
                                    <!-- <input type="checkbox" onclick="myFunction()"><label style="margin: 05 2 0.5em">Show Password</label>  -->  
                                    <!-- <p id="lab"><input type='checkbox' onclick="myFunction()"/> Show Password</p> -->
                                </div>
                                
                                <div class="mt-5">
                                    <button type="submit" class="btn btn-block btn-lg font-weight-medium" style="color: #fff;background: linear-gradient(88deg, #0064D3, #6DC0FF);border-color: #03a9f3;">Login</button>
                                    <br>
                                    <span class="text-muted d-block text-center  justify-center ">Copyright © 2020 <a class="text-success" href="#">Zinggodevelops</a>. All rights reserved.</span>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
            </div>
            <!-- row ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?= base_url(); ?>asset/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url(); ?>asset/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url(); ?>asset/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>asset/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?= base_url(); ?>asset/js/off-canvas.js"></script>
    <script src="<?= base_url(); ?>asset/js/hoverable-collapse.js"></script>
    <script src="<?= base_url(); ?>asset/js/misc.js"></script>
    <script src="<?= base_url(); ?>asset/js/settings.js"></script>
    <script src="<?= base_url(); ?>asset/js/todolist.js"></script>
    <!-- endinject -->
    <script>
        function myFunction() {
          var x = document.getElementById("exampleInputPassword1");
          if (x.type === "password") {
            x.type = "text";
          } else {
            x.type = "password";
          }
        }
    </script>
    <style>
        .lab{
            font-size: 10mm
        }
    </style>
</body>

</html>